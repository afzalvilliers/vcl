# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_200_926_174_112) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'user_permissions', force: :cascade do |t|
    t.bigint 'user_id', null: false
    t.string 'approval_type'
    t.integer 'sequence'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index %w[user_id approval_type], name: 'index_user_permissions_on_user_id_and_approval_type', unique: true
    t.index ['user_id'], name: 'index_user_permissions_on_user_id'
  end

  create_table 'users', force: :cascade do |t|
    t.string 'name'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
  end

  create_table 'work_flow_levels', force: :cascade do |t|
    t.bigint 'work_flow_id', null: false
    t.bigint 'user_permission_id', null: false
    t.string 'approval_action'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['user_permission_id'], name: 'index_work_flow_levels_on_user_permission_id'
    t.index ['work_flow_id'], name: 'index_work_flow_levels_on_work_flow_id'
  end

  create_table 'work_flows', force: :cascade do |t|
    t.string 'approval_type', array: true
    t.string 'status'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
  end

  add_foreign_key 'user_permissions', 'users'
  add_foreign_key 'work_flow_levels', 'user_permissions'
  add_foreign_key 'work_flow_levels', 'work_flows'
end
