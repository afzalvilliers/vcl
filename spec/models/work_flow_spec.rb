# frozen_string_literal: true

RSpec.describe WorkFlow, type: :model do
  it 'Should create workflow with valid approval_type' do
    approval_types = Constant::APPROVAL_TYPE_LISTS
    10.times do |_i|
      WorkFlow.create(approval_type: approval_types.keys)
    end
    expect(WorkFlow.count).to eq(10)
  end

  describe 'Fail cases' do
    it 'Should no create workflow without valid approval_type' do
      expect { WorkFlow.create(approval_type: :any_fake) }.to raise_error(ArgumentError)
      expect(WorkFlow.count).to eq(0)
    end

    it 'Should no create workflow with empty approval_type' do
      work_flow = WorkFlow.create
      expect(work_flow.errors.messages[:approval_type]).to eq(["can't be blank"])
      expect(WorkFlow.count).to eq(0)
    end
  end
end
