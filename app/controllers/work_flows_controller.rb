# frozen_string_literal: true

class WorkFlowsController < ApplicationController
  before_action :set_work_flow, only: %i[show update destroy]

  # GET /work_flows
  def index
    @work_flows = WorkFlow.all

    render json: @work_flows
  end

  # GET /work_flows/1
  def show
    render json: @work_flow
  end

  # POST /work_flows
  def create
    @work_flow = WorkFlow.new(work_flow_params)

    if @work_flow.save
      render json: @work_flow, status: :created, location: @work_flow
    else
      render json: @work_flow.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /work_flows/1
  def update
    if @work_flow.update(work_flow_params)
      render json: @work_flow
    else
      render json: @work_flow.errors, status: :unprocessable_entity
    end
  end

  # DELETE /work_flows/1
  def destroy
    @work_flow.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_work_flow
    @work_flow = WorkFlow.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def work_flow_params
    params.require(:work_flow).permit(:approval_type, :status)
  end
end
