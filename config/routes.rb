# frozen_string_literal: true

Rails.application.routes.draw do
  resources :work_flow_levels
  resources :work_flows
  resources :user_permissions
  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
