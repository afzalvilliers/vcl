# frozen_string_literal: true

RSpec.describe WorkFlowLevel, type: :model do
  it 'Should not create workflow without work_flow_id, user_id, approval_action' do
    bad_level = WorkFlowLevel.create
    expect(bad_level.errors.messages.values.flatten.uniq).to eq(['must exist'])

    user = User.create(name: Faker::Name.name)
    user_perm = user.user_permissions.create(approval_type: :round_robin)
    work_flow = WorkFlow.create(approval_type: :round_robin)
    good_level = WorkFlowLevel.create(user_permission_id: user_perm.id, work_flow_id: work_flow.id)
    expect(good_level.user_permission_id).to eq(user_perm.id)
    expect(good_level.work_flow_id).to eq(work_flow.id)
    expect(good_level.approval_action).to eq('initiated')
  end

  describe 'Negative Flow' do
    it 'Should create user, user_permission, work_flow, work_flow_level' do
      user_1 = User.create(name: 'Elsa Ingram')
      user_perm_1 = user_1.user_permissions.create!(approval_type: :sequential, sequence: 1)

      user_2 = User.create(name: 'Paul Marsh')
      user_perm_2 = user_2.user_permissions.create!(approval_type: :round_robin)

      user_3 = User.create(name: 'D Joshi')
      user_perm_3 = user_3.user_permissions.create!(approval_type: :round_robin)

      user_4 = User.create(name: 'Nick Holden')
      user_perm_4 = user_4.user_permissions.create!(approval_type: :sequential, sequence: 2)
      user_perm_5 = user_4.user_permissions.create!(approval_type: :any_one)

      user_5 = User.create(name: 'John')
      user_perm_6 = user_5.user_permissions.create!(approval_type: :round_robin)
      user_perm_7 = user_5.user_permissions.create!(approval_type: :any_one)

      # work_flow = WorkFlow.create(approval_type: %i[sequential round_robin any_one])
      work_flow = WorkFlow.create(approval_type: %i[sequential round_robin any_one])
      work_flow_levels = work_flow.work_flow_levels
      expect(work_flow_levels.count).to eq(7)

      # Level-1 Sequential
      # User: Elsa Ingram, Elsa Ingram
      work_flow_levels.find_by(user_permission_id: user_perm_1.id).mark_approved!
      work_flow_levels.find_by(user_permission_id: user_perm_4.id).mark_approved!

      # Level-2 Round-Robin
      # User: Paul Marsh, D Joshi, John
      work_flow_levels.find_by(user_permission_id: user_perm_2.id).mark_approved!
      work_flow_levels.find_by(user_permission_id: user_perm_3.id).mark_approved!
      work_flow_levels.find_by(user_permission_id: user_perm_6.id).mark_rejected!

      # Level-3 any-one
      # User: Nick Holden, John
      any_one_id = [user_perm_7.id, user_perm_5.id].sample
      work_flow_levels.find_by(user_permission_id: any_one_id).mark_approved!

      expect(work_flow.status).to eq('terminated')
    end
  end

  describe 'Happy Flow' do
    it 'Should create user, user_permission, work_flow, work_flow_level' do
      user_1 = User.create(name: 'Elsa Ingram')
      user_perm_1 = user_1.user_permissions.create!(approval_type: :sequential, sequence: 1)

      user_2 = User.create(name: 'Paul Marsh')
      user_perm_2 = user_2.user_permissions.create!(approval_type: :round_robin)

      user_3 = User.create(name: 'D Joshi')
      user_perm_3 = user_3.user_permissions.create!(approval_type: :round_robin)

      user_4 = User.create(name: 'Nick Holden')
      user_perm_4 = user_4.user_permissions.create!(approval_type: :sequential, sequence: 2)
      user_perm_5 = user_4.user_permissions.create!(approval_type: :any_one)

      user_5 = User.create(name: 'John')
      user_perm_6 = user_5.user_permissions.create!(approval_type: :round_robin)
      user_perm_7 = user_5.user_permissions.create!(approval_type: :any_one)

      # work_flow = WorkFlow.create(approval_type: %i[sequential round_robin any_one])
      work_flow = WorkFlow.create(approval_type: %i[sequential any_one round_robin])
      work_flow_levels = work_flow.work_flow_levels
      expect(work_flow_levels.count).to eq(7)

      # Level-1 Sequential
      # User: Elsa Ingram, Elsa Ingram
      work_flow_levels.find_by(user_permission_id: user_perm_1.id).mark_approved!
      work_flow_levels.find_by(user_permission_id: user_perm_4.id).mark_reject_and_remove_from_workflow!

      # Level-2 any-one
      # User: Nick Holden, John
      any_one_id = [user_perm_7.id, user_perm_5.id].sample
      work_flow_levels.find_by(user_permission_id: any_one_id).mark_approved!

      # Level-3 Round-Robin
      # User: Paul Marsh, D Joshi, John
      work_flow_levels.find_by(user_permission_id: user_perm_2.id).mark_approved!
      work_flow_levels.find_by(user_permission_id: user_perm_3.id).mark_approved!
      work_flow_levels.find_by(user_permission_id: user_perm_6.id).mark_approved!

      expect(work_flow.status).to eq('executed')
    end
  end

  describe 'Random flow' do
    it 'Should not mark approve to inproper sequence' do
      user_1 = User.create(name: 'Elsa Ingram')
      user_perm_1 = user_1.user_permissions.create!(approval_type: 'sequential', sequence: 2)

      user_2 = User.create(name: 'Nick Holden')
      user_perm_2 = user_2.user_permissions.create!(approval_type: 'sequential', sequence: 1)

      work_flow = WorkFlow.create(approval_type: %i[sequential])
      work_flow_levels = work_flow.work_flow_levels
      expect(work_flow_levels.count).to eq(2)

      # Level-1 Sequential
      # User: Elsa Ingram, Elsa Ingram
      expect { work_flow_levels.find_by(user_permission_id: user_perm_1.id).mark_approved! }.to raise_error
      expect(work_flow.status).to eq('terminated')

      # Valid scenario
      work_flow_levels.find_by(user_permission_id: user_perm_2.id).mark_approved!
      work_flow_levels.find_by(user_permission_id: user_perm_1.id).mark_reject_and_remove_from_workflow!
      expect(work_flow.status).to eq('executed')
    end
  end
end
