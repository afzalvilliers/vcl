# frozen_string_literal: true

class WorkFlow < ApplicationRecord
  include AASM
  extend ArrayEnum

  validates :approval_type, presence: true, allow_nil: false
  array_enum approval_type: Constant::APPROVAL_TYPE_LISTS

  has_many :work_flow_levels

  after_commit :create_levels

  aasm column: :status do
    state :active, initial: true
    state :active, :executed, :terminated

    event :mark_terminated do
      transitions from: %i[active executed terminated], to: :terminated
    end

    event :mark_executed do
      transitions from: %i[active executed terminated], to: :executed
    end
  end

  def approval_types
    approval_type_in_database
  end

  private

  def create_levels
    approval_types.each do |at|
      user_perms = UserPermission.where(approval_type: at).order(sequence: :asc)
      user_perms.each do |user_perm|
        next if work_flow_levels.find_by(user_permission_id: user_perm.id)

        work_flow_levels.create!(user_permission_id: user_perm.id)
      end
    end
  end
end
