# frozen_string_literal: true

class WorkFlowLevelsController < ApplicationController
  before_action :set_work_flow_level, only: %i[show update destroy]

  # GET /work_flow_levels
  def index
    @work_flow_levels = WorkFlowLevel.all

    render json: @work_flow_levels
  end

  # GET /work_flow_levels/1
  def show
    render json: @work_flow_level
  end

  # POST /work_flow_levels
  def create
    @work_flow_level = WorkFlowLevel.new(work_flow_level_params)

    if @work_flow_level.save
      render json: @work_flow_level, status: :created, location: @work_flow_level
    else
      render json: @work_flow_level.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /work_flow_levels/1
  def update
    if @work_flow_level.update(work_flow_level_params)
      render json: @work_flow_level
    else
      render json: @work_flow_level.errors, status: :unprocessable_entity
    end
  end

  # DELETE /work_flow_levels/1
  def destroy
    @work_flow_level.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_work_flow_level
    @work_flow_level = WorkFlowLevel.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def work_flow_level_params
    params.require(:work_flow_level).permit(:work_flow_id, :user_id, :approval_action)
  end
end
