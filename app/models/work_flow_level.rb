# frozen_string_literal: true

class WorkFlowLevel < ApplicationRecord
  include AASM
  belongs_to :work_flow
  belongs_to :user_permission

  scope :only_approved_actions, -> { where(approval_action: %i[approved reject_and_remove_from_workflow]) }
  scope :only_unapproved_actions, -> { where.not(approval_action: %i[approved reject_and_remove_from_workflow]) }

  after_commit :change_work_flow_status

  aasm column: :approval_action do
    state :initiated, initial: true
    state :approved, :rejected, :reject_and_remove_from_workflow

    event :mark_approved do
      before do
        sequential_scenario if user_permission.approval_type == 'sequential'
      end
      transitions to: :approved
    end

    event :mark_rejected do
      transitions to: :rejected
    end

    event :mark_reject_and_remove_from_workflow do
      transitions to: :reject_and_remove_from_workflow
    end
  end

  def sequential_scenario
    sequential_lists = UserPermission.sequentials
    current_seq = sequential_lists.find(user_permission_id)
    prev_seq = sequential_lists.where('sequence < ?', current_seq.sequence)
    halt = prev_seq && WorkFlowLevel.where(work_flow_id: work_flow_id, user_permission_id: prev_seq.pluck(:id)).only_unapproved_actions
    raise 'Unable to approve the record as, Please approve in sequence' if halt.present?
  end

  private

  def change_work_flow_status
    if check_levels
      work_flow.mark_executed!
    else
      work_flow.mark_terminated!
    end
  end

  def check_levels
    check_list = []
    work_flow.approval_types.each do |approval_type|
      check_list << send(approval_type + '_completed!')
    end
    check_list.all?(false)
  end

  def sequential_completed!
    sequential_lists = UserPermission.sequentials
    WorkFlowLevel.where(work_flow_id: work_flow_id, user_permission_id: sequential_lists.pluck(:id)).only_unapproved_actions.present?
  end

  def round_robin_completed!
    round_robin_lists = UserPermission.round_robin
    WorkFlowLevel.where(work_flow_id: work_flow_id, user_permission_id: round_robin_lists.pluck(:id)).only_unapproved_actions.present?
  end

  def any_one_completed!
    any_one_lists = UserPermission.any_one
    any_one_records = WorkFlowLevel.where(work_flow_id: work_flow_id, user_permission_id: any_one_lists.pluck(:id)).pluck(:approval_action)
    any_one_records.none?('approved') && any_one_records.any?('rejected')
  end
end
