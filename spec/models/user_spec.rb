# frozen_string_literal: true

RSpec.describe User, type: :model do
  it 'Should create uniq name' do
    10.times do |_i|
      User.create(name: Faker::Name.name + "-#{_i}")
    end
    expect(User.count).to eq(10)
  end

  it 'It should not save empty and duplicate record' do
    user_1 = User.create(name: nil)
    user_2 = User.create(name: 'John')
    user_3 = User.create(name: 'John')
    expect(user_1.errors.messages[:name]).to eq(["can't be blank"])
    expect(user_2.name).to eq('John')
    expect(user_3.errors.messages[:name]).to eq(['has already been taken'])
  end
end
