# frozen_string_literal: true

class CreateWorkFlows < ActiveRecord::Migration[6.0]
  def change
    create_table :work_flows do |t|
      t.string :approval_type, array: true
      t.string :status

      t.timestamps
    end
  end
end
