# frozen_string_literal: true

class User < ApplicationRecord
  validates :name, presence: true, uniqueness: { scope: :name }, allow_nil: false
  has_many :user_permissions
end
