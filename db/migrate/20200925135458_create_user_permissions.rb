# frozen_string_literal: true

class CreateUserPermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :user_permissions do |t|
      t.references :user, null: false, foreign_key: true
      t.string :approval_type
      t.integer :sequence

      t.timestamps
    end
    add_index :user_permissions, %i[user_id approval_type], unique: true
  end
end
