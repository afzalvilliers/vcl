# frozen_string_literal: true

class CreateWorkFlowLevels < ActiveRecord::Migration[6.0]
  def change
    create_table :work_flow_levels do |t|
      t.references :work_flow, null: false, foreign_key: true
      t.references :user_permission, null: false, foreign_key: true
      t.string :approval_action

      t.timestamps
    end
  end
end
