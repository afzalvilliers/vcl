# frozen_string_literal: true

class UserPermission < ApplicationRecord
  include StringEnums

  belongs_to :user
  validates :approval_type, presence: true, uniqueness: { scope: :user_id }, allow_nil: false
  validates :sequence, presence: true, uniqueness: { scope: %i[approval_type] }, allow_nil: false, if: :sequence_check?
  string_enum approval_type: Constant::APPROVAL_TYPE_LISTS.keys

  scope :sequentials, -> { where(approval_type: :sequential).order(sequence: :asc) }
  scope :round_robin, -> { where(approval_type: :round_robin) }
  scope :any_one, -> { where(approval_type: :any_one) }

  def sequence_check?
    approval_type == 'sequential'
  end
end
