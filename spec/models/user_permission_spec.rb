# frozen_string_literal: true

RSpec.describe UserPermission, type: :model do
  it 'should save valid user_id & approval_type' do
    user = User.create(name: Faker::Name.name)
    user_perm = user.user_permissions.create(approval_type: %i[sequential round_robin any_one].sample)
    if user_perm.approval_type == 'sequential'
      user_perm.sequence = 1
      user_perm.save
    end
    expect(User.count).to eq(1)
    expect(UserPermission.count).to eq(1)
  end

  it 'should not save empty user_id to user_permissions' do
    user_perm = described_class.create(approval_type: :sequential)
    expect(user_perm.errors.messages[:user]).to eq(['must exist'])
    expect(UserPermission.count).to eq(0)
  end

  it 'should not save empty approval_type to user_permissions' do
    user = User.create(name: Faker::Name.name)
    user_perm = user.user_permissions.create(approval_type: nil)
    expect(user_perm.errors.messages[:approval_type]).to eq(["can't be blank"])
  end

  it 'should not save invalid approval_type to user_permissions' do
    user = User.create(name: Faker::Name.name)
    user_perm = user.user_permissions.create(approval_type: :any_fake)
    expect(user_perm.errors.messages[:approval_type]).to eq(["Invalid value for approval_type, possible values are: [\"sequential\", \"round_robin\", \"any_one\"]"])
  end

  it 'should not save uniqu and valid user & approval_type to user_permissions' do
    user = User.create(name: Faker::Name.name)
    user_perm_1 = user.user_permissions.create(approval_type: :any_one)
    user_perm_2 = user.user_permissions.create(approval_type: :any_one)
    user_perm_3 = user.user_permissions.create(approval_type: :round_robin)
    expect(user_perm_1.errors.any?).to be_falsey
    expect(user_perm_2.errors.any?).to be_truthy
    expect(user_perm_2.errors.messages[:approval_type]).to eq(['has already been taken'])
    expect(user_perm_3.errors.any?).to be_falsey
  end

  describe 'Case where approval_type is sequential' do
    it 'It should not save record is sequence is present' do
      user = User.create(name: Faker::Name.name)
      user_perm_1 = user.user_permissions.create(approval_type: :sequential)
      expect(user_perm_1.errors.any?).to be_truthy
      expect(user_perm_1.errors.messages[:sequence]).to eq(["can't be blank"])
    end

    it 'It should not save record with valid sequence' do
      5.times do |_i|
        user = User.create(name: Faker::Name.name)
        user.user_permissions.create(approval_type: :sequential, sequence: _i)
      end
      expect(User.count).to eq(5)
      expect(UserPermission.count).to eq(5)
    end
  end
end
