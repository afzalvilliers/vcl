# frozen_string_literal: true

module Constant
  APPROVAL_TYPE_LISTS = { sequential: :sequential, round_robin: :round_robin, any_one: :any_one }.freeze
  APPROVAL_ACTION_LISTS = { approved: :approved, rejected: :rejected, reject_and_remove_from_workflow: :reject_and_remove_from_workflow }.freeze
end
